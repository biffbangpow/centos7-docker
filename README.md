# Centos 7 Docker container

Intending this to be a public image that will have php56 and php7 tags (maybe
more), the idea is that we can use these as images for testing and even use
them in circle-ci. Currently only the PHP56 image has been built.

## General Container contents

* Docker (1.12)
* Docker compose (1.9)

### 5.6 explicit contents

* PHP 5.6
* composer (latest)

## Building

    docker build --force-rm -t bbpdev/centos7-docker:php56 5.6/

## Pushing

    docker login
    docker push bbpdev/centos7-docker:php56
